%global __requires_exclude %{?__requires_exclude:__requires_exclude|}^perl\\(HTTP::Date\\)\s*$
%global __provides_exclude %{?__provides_exclude:__provides_exclude|}^perl\\(File::Listing::

Name:          perl-File-Listing
Version:       6.16
Release:       2
Summary:       Parse directory listing
License:       GPL-1.0-or-later OR Artistic-1.0-Perl
URL:           https://metacpan.org/release/File-Listing
Source0:       https://cpan.metacpan.org/authors/id/P/PL/PLICEASE/File-Listing-%{version}.tar.gz
BuildArch:     noarch

BuildRequires: perl-interpreter perl-generators perl(Carp) perl(Exporter) perl(Time::Local) perl(Test)
BuildRequires: perl(ExtUtils::MakeMaker) perl(HTTP::Date) >= 6 perl(strict) perl(vars) perl(Test::More)

Requires:      perl(HTTP::Date) >= 6 perl(Time::Local)
Conflicts:     perl-libwww-perl < 6

%description
This module exports a single function called parse_dir(), which can be used to parse directory listings.

%prep
%autosetup -n File-Listing-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor
%make_build

%install
make pure_install DESTDIR=$RPM_BUILD_ROOT
%{_fixperms} $RPM_BUILD_ROOT/*

%check
make test

%files
%doc Changes README
%{perl_vendorlib}/*
%{_mandir}/man3/*

%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 6.16-2
- drop useless perl(:MODULE_COMPAT) requirement

* Wed Sep 20 2023 zhaoshuang <tc@openeuler.org> - 6.16-1
- Upgrade to version 6.16

* Tue Jun 14 2022 SimpleUpdate Robot <tc@openeuler.org> - 6.15-1
- Upgrade to version 6.15

* Tue Dec 14 2021 guozhaorui <guozhaorui1@huawei.com> - 6.14-1
- update version to 6.14

* Tue Oct 22 2019 Huiming Xie <xiehuiming@huawei.com> - 6.04-20
- init package
